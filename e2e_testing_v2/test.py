
import time
import random
import serial
import asyncio
import unittest

from syncer import sync
from pyppeteer import launch


arduino_port = "/dev/ttyACM0";
arduino_site = "http://192.168.2.172";

opts = {
    'args': ['--no-sandbox', '--disable-setuid-sandbox'],
    'headless': True,
    #'slowmo': 100
    'timeout': 10000
}

port = serial.Serial(arduino_port, 9600, timeout=2)

def read_arduino():
    data = ''
    line = port.readline().decode();
    data += line
    while line:
        line = port.readline().decode()
        data += line
    return data

async def post_mode(page):
    await page.setRequestInterception(True)
    page.once('request', lambda request: request.continue_({'method': 'POST'}))

class TestMain(unittest.TestCase):

    @sync
    async def setup(self):
        self.browser = await launch(opts)
        self.page = await self.browser.newPage()
        await self.page.goto(arduino_site)

        time.sleep(1);
        read_arduino(); #Clear the page loaded messages

    @sync
    async def disable_post(self):
        await self.page.setRequestInterception(False);

    @sync
    async def test_reset_arduino(self):
        await post_mode(self.page);

        response = await self.page.goto(arduino_site+'/reset')

    @sync
    async def test_poll_status_unknown(self):
        await post_mode(self.page);

        response = await self.page.goto(arduino_site+'/poll')
        self.assertEqual('Unknown', await response.text())

    @sync
    async def test_form_checks_constraints(self):
        await self.page.goto(arduino_site)

    @sync
    async def test_submitting_form(self):
        await self.page.goto(arduino_site)

        red = random.randint(0,5);
        green = random.randint(0,5);
        blue = random.randint(0,5);

        await self.page.evaluate("() => { document.querySelector('[name=red]').value = '%d' }" % (red));
        await self.page.evaluate("() => { document.querySelector('[name=green]').value = '%d' }" % (green));
        await self.page.evaluate("() => { document.querySelector('[name=blue]').value = '%d' }" % (blue));
        await self.page.click('#start');

        response = read_arduino();
        self.assertIn("Start API called", response)
        self.assertIn("Red %d, Green: %d, Blue: %d" % (red,green,blue), response)

    @sync
    async def test_poll_status_start(self):
        await post_mode(self.page);

        response = await self.page.goto(arduino_site+'/poll')
        self.assertEqual('Starting', await response.text())

    @sync
    async def test_stop(self):
        await self.page.goto(arduino_site)
        await self.page.click('#stop');

        response = read_arduino();
        self.assertIn("Stop API called", response)

    @sync
    async def test_poll_status_stop(self):
        await post_mode(self.page);
        response = await self.page.goto(arduino_site+'/poll')
        self.assertEqual('Stopping', await response.text())

    @sync
    async def teardown(self):
        await self.browser.close()

if __name__ == "__main__":
    time.sleep(1);
    read_arduino(); #Clear the initial setup messages
    
    print("Setting up testing environment")
    test_main = TestMain()
    test_main.setup();
    print("Finished setting up environment")
    print()

    tests = ['test_reset_arduino',
            'test_poll_status_unknown',
            'test_form_checks_constraints',
            'test_submitting_form',
            'test_poll_status_start',
            'test_stop',
            'test_poll_status_stop']
    all_tests = list(filter(lambda x: 'test_' in x, dir(test_main)))

    if (len(all_tests) != len(tests)):
        print("Test function amount mismatch: You are probably missing a test in the tests list")

    for test in tests:
        test_main.disable_post();
        print("Running test: "+test)
        getattr(test_main,test)();
        print("Test: "+test+" succeeded")
        print()

    test_main.teardown();

