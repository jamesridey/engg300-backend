 /*
 * Web Server - multi-page.  5 Feb 2011.
 * Need an Ethernet Shield over Arduino.
 *
 * by Martyn Woerner extending the good work of
 * Alessandro Calzavara, alessandro(dot)calzavara(at)gmail(dot)com
 * and Alberto Capponi, bebbo(at)fast-labs net
 * for Arduino community! :-)
 * 
 * Pro:
 * - HTTP Requests GET & POST
 * - Switch page selection.
 * - HTML pages in flash memory.
 * - Button to turn LED on/off
 * - Favicon & png images
 *
 */
#include <SPI.h>
#include <Ethernet.h>
#include "WebServer.h"
#include "BluetoothInterface.h"

//#define USE_DHCP_FOR_IP_ADDRESS
#define USE_IMAGES
#define USE_ARDUINO_ICON

/**********************************************************************************************************************
*                                   MAC address and IP address.
***********************************************************************************************************************/
byte mac[] = { 0xD2, 0x75, 0xC7, 0x40, 0x8F, 0x0F };

#if !defined USE_DHCP_FOR_IP_ADDRESS
// ip represents the fixed IP address to use if DHCP is disabled.
byte ip[] = { 192, 168, 1, 1 };
#endif

// Http header token delimiters
const char *pSpDelimiters = " \r\n";
const char *pStxDelimiter = "\002";    // STX - ASCII start of text character

/**********************************************************************************************************************
*                                   Strings stored in flash of the HTML we will be transmitting
***********************************************************************************************************************/

// HTTP Request message
const char content_404[] PROGMEM = "HTTP/1.1 404 Not Found\nServer: arduino\nContent-Type: text/html\n\n<html><head><title>Arduino Web Server - Error 404</title></head><body><h1>Error 404: Sorry, that page cannot be found!</h1></body>";
const char * const page_404[] PROGMEM = { content_404 }; // table with 404 page

// HTML Header for pages
const char content_main_header[] PROGMEM = "HTTP/1.0 200 OK\nServer: arduino\nCache-Control: no-store, no-cache, must-revalidate\nPragma: no-cache\nConnection: close\nContent-Type: text/html\n";
const char content_main_top[] PROGMEM = "<html><head><title>Arduino Web Server</title><style type=\"text/css\">table{border-collapse:collapse;}td{padding:0.25em 0.5em;border:0.5em solid #C8C8C8;}</style>"
                                        "<link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\"/><link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\"/></head><body><h1>Arduino Web Server</h1>";
const char content_main_footer[] PROGMEM = "</body></html>";
const char * const contents_main[] PROGMEM = { content_main_header, content_main_top, content_main_footer }; // table with 404 page
#define CONT_HEADER 0
#define CONT_TOP 1
#define CONT_MENU 2
#define CONT_FOOTER 3

// Page 1
const char http_uri1[] PROGMEM = "/";
const char content_title1[] PROGMEM = "<h2>Can selection</h2>";
const char content_page1[] PROGMEM = "<html><head><style type=\"text/css\">body{font-size:24px; color:#3a3309;font-weight:bold; margin: 43px; background: #d6d2be; font-family: arial; font-style: oblique;}}</style><script>function poll(){var req=new XMLHttpRequest();req.open('POST', '/poll', true);req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');req.send('');req.onreadystatechange=function(){if (req.readyState==4 && req.status==200){var data=req.responseText;document.getElementById(\"status\").innerText=data;}};setTimeout(poll,3000);}window.onload=poll;function myFunction(){var redCans=parseInt(document.getElementById(\"RED\").value); var greenCans=parseInt(document.getElementById(\"GREEN\").value); var blueCans=parseInt(document.getElementById(\"BLUE\").value); if(!Number.isInteger(redCans)){document.getElementById(\"RED\").value=0; redCans=0;}if(!Number.isInteger(greenCans)){document.getElementById(\"GREEN\").value=0; greenCans=0;}if(!Number.isInteger(blueCans)){document.getElementById(\"BLUE\").value=0; blueCans=0;}if(redCans+greenCans+blueCans >=11){document.getElementById(\"error\").innerText=\"Numbers of cans exceeds 10\";return false;}if(redCans < 0 || greenCans < 0 || blueCans < 0){document.getElementById(\"error\").innerText=\"Can't Have Negative Cans\";return false;}document.getElementById(\"red\").innerText=\"Red: \"+redCans;document.getElementById(\"gree\").innerText=\"Green: \"+greenCans;document.getElementById(\"blu\").innerText=\"Blue: \"+blueCans;document.getElementById(\"error\").innerText=\"\";return true;}</script></head><iframe id=\"iframe\" name=\"my_iframe\" style=\"display: none\"></iframe><form method=\"post\" target=\"my_iframe\"><br>Updates:<p style=\"color:blue\" id=\"status\"></p><p style=\"color:red\" id=\"error\"></p><p id=\"red\">Red cans: </p><p id=\"gree\">Green cans: </p><p id=\"blu\">Blue cans: </p><br><br><br>Please select your colours - <br><br>Red: <input id=\"RED\" type=\"number\" max=\"10\" min=\"0\" name=\"red\" placeholder=\"-\"><br/>Green: <input id=\"GREEN\" type=\"number\" max=\"10\" min=\"0\" name=\"green\" placeholder=\"-\"><br/>Blue: <input id=\"BLUE\" type=\"number\" max=\"10\" min=\"0\" name=\"blue\" placeholder=\"-\"><br/><br><button type=\"submit\" formaction=\"/start\" onclick=\"return myFunction()\">Start</button><button type=\"submit\" formaction=\"/stop\">Stop</button></form></html>";

// declare tables for the pages
const char * const contents_titles[] PROGMEM = { content_title1 }; // titles
const char * const contents_pages[] PROGMEM  = { content_page1 }; // real content

// API Responses
const char api_response[] PROGMEM = "OK";
const char api_response_error[] PROGMEM = "ERROR";

// API Urls
const char http_api1[] PROGMEM = "/start";
const char http_api2[] PROGMEM = "/stop";
const char http_api3[] PROGMEM = "/poll";
const char http_api4[] PROGMEM = "/reset";

const char * const api_pages[] PROGMEM  = { http_api1, http_api2, http_api3, http_api4 };

//TODO: There is probably a better way of doing this
#ifdef USE_IMAGES
const char favicon_uri[] PROGMEM = "/favicon.ico";    // favicon Request message
const char * const http_uris[] PROGMEM = { http_uri1, http_api1, http_api2, http_api3, http_api4, favicon_uri };
#else
const char * const http_uris[] PROGMEM = { http_uri1, http_api1, http_api2, http_api3, http_api4 };
#endif

// declare tables for apis
enum ApiEnum {
  INVALID = 1,
  START = 2,
  STOP = 3,
  POLL = 4,
  RESET = 5,
};
ApiEnum const api_content[] = { INVALID, START, STOP, POLL, RESET };

#ifdef USE_IMAGES

/**********************************************************************************************************************
*                     Image strings and data stored in flash for the image UISs we will be transmitting
***********************************************************************************************************************/

// A Favicon is a little custom icon that appears next to a website's URL in the address bar of a web browser.
// They also show up in your bookmarked sites, on the tabs in tabbed browsers, and as the icon for Internet shortcuts
// on your desktop or other folders in Windows.

const char content_image_header[] PROGMEM = "HTTP/1.1 200 OK\nServer: arduino\nContent-Length: \002\nContent-Type: image/\002\n\r\n";

#ifdef USE_ARDUINO_ICON
const unsigned char content_favicon_data[] PROGMEM = {
             0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x10, 0x10, 0x10, 0x00, 0x01, 0x00, 0x04, 0x00, 0x28, 0x01, 0x00, 0x00, 0x16, 0x00,
             0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x80, 0x80, 0x00, 0x00, 0x80, 0x80, 0x80, 0x00, 0xC0, 0xC0, 0xC0, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44, 0x44, 0x41, 0x00, 0x00, 0x14, 0x44, 0x44, 0x44, 0x41, 0x00, 0x00, 0x00, 0x00,
             0x14, 0x44, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x40, 0x23,
             0x33, 0x20, 0x02, 0x33, 0x32, 0x04, 0x12, 0x30, 0x00, 0x32, 0x23, 0x00, 0x03, 0x21, 0x03, 0x00, 0x00, 0x03, 0x30, 0x03,
             0x00, 0x30, 0x03, 0x03, 0x33, 0x03, 0x30, 0x33, 0x30, 0x30, 0x03, 0x00, 0x00, 0x03, 0x30, 0x03, 0x00, 0x30, 0x02, 0x30,
             0x00, 0x32, 0x23, 0x00, 0x03, 0x20, 0x10, 0x23, 0x33, 0x20, 0x02, 0x33, 0x32, 0x01, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x04, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44, 0x44, 0x41,
             0x00, 0x00, 0x00, 0x00, 0x14, 0x44, 0x44, 0x44, 0x41, 0x00, 0x00, 0x14, 0x44, 0x44, 0xF8, 0x1F, 0x00, 0x00, 0xE0, 0x07,
             0x00, 0x00, 0xC0, 0x03, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01,
             0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0xC0, 0x03, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0xF8, 0x1F, 0x00, 0x00
             };
#else

const unsigned char content_favicon_data[] PROGMEM = {
             0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x10, 0x10, 0x10, 0x00, 0x01, 0x00, 0x04, 0x00, 0x28, 0x01, 0x00, 0x00, 0x16, 0x00,
             0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x66, 0x00, 0x00, 0x33, 0x66, 0x00, 0x33, 0x33, 0x66, 0x00, 0x66, 0x66, 0x99, 0x00, 0x99, 0x99,
             0x99, 0x00, 0x99, 0x99, 0xCC, 0x00, 0xCC, 0xCC, 0xCC, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x62, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x76, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x77, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x26, 0x63, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x50, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x07, 0x72, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x37, 0x75, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x76, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x77, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x77, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x37, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
             };
#endif  // USE_ARDUINO_ICON

// declare tables for the images
const char * const image_header PROGMEM = content_image_header;
const unsigned char * const data_for_images[] PROGMEM = { content_favicon_data }; // real data
const int size_for_images[] PROGMEM = { sizeof(content_favicon_data) };

#else

// declare tables for the images
const char * const image_header PROGMEM = NULL;
const char * const data_for_images[] PROGMEM = { }; // real data
const int size_for_images[] PROGMEM = { };

#endif // USE_IMAGES

#define NUM_PAGES  sizeof(contents_pages)  / sizeof(contents_pages[0])
#define NUM_APIS   sizeof(api_pages)       / sizeof(api_pages[0])
#define NUM_IMAGES sizeof(data_for_images) / sizeof(data_for_images[0])    // favicon or png format
#define NUM_URIS  (NUM_APIS + NUM_PAGES + NUM_IMAGES)  // Pages URIs + favicon URI, etc

//State of the robot
BluetoothInterface currentBluetoothState;

//Forward declaration
void sendProgMemAsBinary(EthernetClient & client, const char* realword, int realLen, bool progMem = true);
MethodType readHttpRequest(EthernetClient & client, int & nUriIndex, BUFFER & requestContent);
void sendProgMemAsString(EthernetClient & client, const char *realword);
void apiRequest(EthernetClient& client, int nUriIndex, BUFFER& requestContent);
void sendPage(EthernetClient & client, int nUriIndex, BUFFER & requestContent);
void sendImage(EthernetClient & client, int nUriIndex, BUFFER & requestContent);
MethodType readRequestLine(EthernetClient & client, BUFFER & readBuffer, int & nUriIndex, BUFFER & requestContent);
void readRequestHeaders(EthernetClient & client, BUFFER & readBuffer, int & nContentLength, bool & bIsUrlEncoded);
void readEntityBody(EthernetClient & client, int nContentLength, BUFFER & content);
void getNextHttpLine(EthernetClient & client, BUFFER & readBuffer);
int GetUriIndex(char * pUri);
void sendUriContentByIndex(EthernetClient client, int nUriIndex, BUFFER & requestContent);
void sendSubstitute(EthernetClient client, int nUriIndex, int nSubstituteIndex, BUFFER & requestContent);


/**********************************************************************************************************************
*                                                 Shared variable and Setup()
***********************************************************************************************************************/
EthernetServer server(80);

void setup()
{
  beginBluetooth(9600);
  Serial.println(F("Trying to connect to Bluetooth."));
  while (!getConnectionStatus()) {
    connectBluetooth();
    if (!getConnectionStatus()) Serial.println(F("Retrying."));
  }
  Serial.println(F("Successfully connected to Bluetooth"));
 
  Serial.println(F("Starting Server."));
  Serial.println(F("Obtaining Ethernet Address..."));
#ifdef USE_DHCP_FOR_IP_ADDRESS
    Ethernet.begin(mac);  // Use DHCP to get an IP address
#else
    Ethernet.begin(mac, ip);
#endif

  delay(250);  
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}

/**********************************************************************************************************************
*                                                           Main loop
***********************************************************************************************************************/

void loop()
{
  EthernetClient client = server.available();

  if (client)
  {
    // now client is connected to arduino we need to extract the
    // following fields from the HTTP request.
    int    nUriIndex;  // Gives the index into table of recognized URIs or -1 for not found.
    BUFFER requestContent;    // Request content as a null-terminated string.
    MethodType eMethod = readHttpRequest(client, nUriIndex, requestContent);

    Serial.print("Read Request type: ");
    Serial.print(eMethod);
    Serial.print(" Uri index: ");
    Serial.print(nUriIndex);
    Serial.print(" content: ");
    Serial.println(requestContent);

    if (nUriIndex < 0)
    {
      // URI not found
      Serial.println(F("Not found"));
      sendProgMemAsString(client, (char*)pgm_read_word(&(page_404[0])));
    }
    else if (nUriIndex >= NUM_PAGES && nUriIndex < NUM_PAGES+NUM_APIS && eMethod == MethodPost)
    {
      //Post request
      Serial.println(F("Api"));
      apiRequest(client, nUriIndex, requestContent);
    }
    else if (nUriIndex < NUM_PAGES)
    {
      // Normal page request, may depend on content of the request
      Serial.println(F("Normal page"));
      sendPage(client, nUriIndex, requestContent);
    }
    else
    {
      // Image request
      Serial.println(F("Image"));
      sendImage(client, nUriIndex, requestContent);
    }

    // give the web browser time to receive the data
    delay(1);
    client.stop();
  }
  
  if (receivedNewData()) {
    Serial.println(F("Information received from robot"));  
  }


  //If we are not connected and we haven't already tried 5 times, try to reconnect
//  if (currentBluetoothState.getStatus() != ERROR_BLUETOOTH && !getConnectionStatus()) {
//    currentBluetoothState.setStatus(ERROR_BLUETOOTH);
//
//    //Retry connecting to bluetooth, 5 times
//    for (int i = 0; i <= 5; i++) {
//      Serial.println(F("Bluetooth is not connected, trying to connect"));
//      connectBluetooth();
//
//      //If we get a connection reset the state to unknown
//      if (getConnectionStatus()) {
//        currentBluetoothState.setStatus(UNKNOWN);
//        break;
//      }
//    }
//  }
}

//Extract the parameters from the url
BluetoothInterface decodeContent(BluetoothInterface& b, BUFFER& requestContent) {
  char* savePtr1;
  char* parameter = strtok_r(requestContent,"&",&savePtr1);

  while (parameter != NULL) {
    //Split by the & symbol for the parameter name
    char* parameterName = strtok(parameter,"=");
    //Split by the = symbol for the parameter value
    int parameterValue = atoi(strtok(NULL,"="));

    //Just check the first character for red,green,blue is case of spelling mistakes
    if (parameterName[0] == 'r') {
      b.setRed(parameterValue);
    } else if (parameterName[0] == 'g') {
      b.setGreen(parameterValue);
    } else if (parameterName[0] == 'b') {
      b.setBlue(parameterValue);
    }

    parameter = strtok_r(NULL,"&",&savePtr1);
  }

  return b;
}

void apiRequest(EthernetClient& client, int nUriIndex, BUFFER& requestContent) { 
  static unsigned int v[3]; 
  
  switch (api_content[nUriIndex]) {
    case START:
      Serial.println(F("Start API called"));
      currentBluetoothState.setStatus(STARTING);
      decodeContent(currentBluetoothState,requestContent);
      Serial.println(F("Contents read"));

      //Print and send the can data
      v[0] = currentBluetoothState.getRed();
      v[1] = currentBluetoothState.getGreen();
      v[2] = currentBluetoothState.getBlue();
      //v[3] = 1;
      
      char s[128];
      sprintf(s,"Red %d, Green: %d, Blue: %d",v[0],v[1],v[2]);
      Serial.println(s);
      
      sendIntArray(v);

      sendProgMemAsString(client, (char*)api_response);
      break;
    case STOP:
      Serial.println(F("Stop API called"));
      currentBluetoothState.setStatus(STOPPING);

      //Send all zeros to bluetooth for the stop command
      v[0] = 0;
      v[1] = 0;
      v[2] = 0;
      //v2[3] = 0;

      sendIntArray(v);
      
      sendProgMemAsString(client, (char*)api_response); 
      break;
    case POLL:
      //Web pages polls the server for the status of the robot
      Serial.println(F("Poll API called"));
      
      char* response;
      response = currentBluetoothState.getStatusAsString();
      Serial.print("Status: ");
      Serial.println(response);

      sendProgMemAsBinary(client, response, strlen(response), false);
      break;
    case RESET:
      //API to reset the webserver if we are unable to connect to bluetooth for example
      Serial.println(F("Reset API called"));
      currentBluetoothState.setStatus(UNKNOWN);
      currentBluetoothState.setCanAmount(0,0,0);

      sendProgMemAsString(client, (char*)api_response);
      break;
    default:
      Serial.println(F("Unknown API called"));
      sendProgMemAsString(client, (char*)api_response_error);
  }
}

/**********************************************************************************************************************
*                                              Method for read HTTP Header Request from web client
*
* The HTTP request format is defined at http://www.w3.org/Protocols/HTTP/1.0/spec.html#Message-Types
* and shows the following structure:
*  Full-Request and Full-Response use the generic message format of RFC 822 [7] for transferring entities. Both messages may include optional header fields
*  (also known as "headers") and an entity body. The entity body is separated from the headers by a null line (i.e., a line with nothing preceding the CRLF).
*      Full-Request   = Request-Line       
*                       *( General-Header 
*                        | Request-Header  
*                        | Entity-Header ) 
*                       CRLF
*                       [ Entity-Body ]    
*
* The Request-Line begins with a method token, followed by the Request-URI and the protocol version, and ending with CRLF. The elements are separated by SP characters.
* No CR or LF are allowed except in the final CRLF sequence.
*      Request-Line   = Method SP Request-URI SP HTTP-Version CRLF
* HTTP header fields, which include General-Header, Request-Header, Response-Header, and Entity-Header fields, follow the same generic format.
* Each header field consists of a name followed immediately by a colon (":"), a single space (SP) character, and the field value.
* Field names are case-insensitive. Header fields can be extended over multiple lines by preceding each extra line with at least one SP or HT, though this is not recommended.     
*      HTTP-header    = field-name ":" [ field-value ] CRLF
***********************************************************************************************************************/
// Read HTTP request, setting Uri Index, the requestContent and returning the method type.
MethodType readHttpRequest(EthernetClient & client, int & nUriIndex, BUFFER & requestContent)
{
  BUFFER readBuffer;    // Just a work buffer into which we can read records
  int nContentLength = 0;
  bool bIsUrlEncoded;

  requestContent[0] = 0;    // Initialize as an empty string
  // Read the first line: Request-Line setting Uri Index and returning the method type.
  MethodType eMethod = readRequestLine(client, readBuffer, nUriIndex, requestContent);
  // Read any following, non-empty headers setting content length.
  readRequestHeaders(client, readBuffer, nContentLength, bIsUrlEncoded);

  if (nContentLength > 0)
  {
  // If there is some content then read it and do an elementary decode.
    readEntityBody(client, nContentLength, requestContent);
    if (bIsUrlEncoded)
    {
    // The '+' encodes for a space, so decode it within the string
    for (char * pChar = requestContent; (pChar = strchr(pChar, '+')) != NULL; )
      *pChar = ' ';    // Found a '+' so replace with a space
    }
  }

  return eMethod;
}

// Read the first line of the HTTP request, setting Uri Index and returning the method type.
// If it is a GET method then we set the requestContent to whatever follows the '?'. For a other
// methods there is no content except it may get set later, after the headers for a POST method.
MethodType readRequestLine(EthernetClient & client, BUFFER & readBuffer, int & nUriIndex, BUFFER & requestContent)
{
  MethodType eMethod;
  // Get first line of request:
  // Request-Line = Method SP Request-URI SP HTTP-Version CRLF
  getNextHttpLine(client, readBuffer);
  // Split it into the 3 tokens
  char * pMethod  = strtok(readBuffer, pSpDelimiters);
  char * pUri     = strtok(NULL, pSpDelimiters);
  char * pVersion = strtok(NULL, pSpDelimiters);
  // URI may optionally comprise the URI of a queryable object a '?' and a query
  // see http://www.ietf.org/rfc/rfc1630.txt
  strtok(pUri, "?");
  char * pQuery   = strtok(NULL, "?");
  if (pQuery != NULL)
  {
    strcpy(requestContent, pQuery);
    // The '+' encodes for a space, so decode it within the string
    for (pQuery = requestContent; (pQuery = strchr(pQuery, '+')) != NULL; )
      *pQuery = ' ';    // Found a '+' so replace with a space

//    Serial.print("Get query string: ");
//    Serial.println(requestContent);
  }
  if (strcmp(pMethod, "GET") == 0)
    eMethod = MethodGet;
  else if (strcmp(pMethod, "POST") == 0)
    eMethod = MethodPost;
  else if (strcmp(pMethod, "HEAD") == 0)
    eMethod = MethodHead;
  else
    eMethod = MethodUnknown;

  // See if we recognize the URI and get its index
  nUriIndex = GetUriIndex(pUri);

  return eMethod;
}

// Read each header of the request till we get the terminating CRLF
void readRequestHeaders(EthernetClient & client, BUFFER & readBuffer, int & nContentLength, bool & bIsUrlEncoded)
{
  nContentLength = 0;      // Default is zero in cate there is no content length.
  bIsUrlEncoded  = true;   // Default encoding
  // Read various headers, each terminated by CRLF.
  // The CRLF gets removed and the buffer holds each header as a string.
  // An empty header of zero length terminates the list.
  do
  {
    getNextHttpLine(client, readBuffer);
//    Serial.println(readBuffer); // DEBUG
    // Process a header. We only need to extract the (optionl) content
    // length for the binary content that follows all these headers.
    // General-Header = Date | Pragma
    // Request-Header = Authorization | From | If-Modified-Since | Referer | User-Agent
    // Entity-Header  = Allow | Content-Encoding | Content-Length | Content-Type
    //                | Expires | Last-Modified | extension-header
    // extension-header = HTTP-header
    //       HTTP-header    = field-name ":" [ field-value ] CRLF
    //       field-name     = token
    //       field-value    = *( field-content | LWS )
    //       field-content  = <the OCTETs making up the field-value
    //                        and consisting of either *TEXT or combinations
    //                        of token, tspecials, and quoted-string>
    char * pFieldName  = strtok(readBuffer, pSpDelimiters);
    char * pFieldValue = strtok(NULL, pSpDelimiters);

    if (strcmp(pFieldName, "Content-Length:") == 0)
    {
      nContentLength = atoi(pFieldValue);
    }
    else if (strcmp(pFieldName, "Content-Type:") == 0)
    {
      if (strcmp(pFieldValue, "application/x-www-form-urlencoded") != 0)
        bIsUrlEncoded = false;
    }
  } while (strlen(readBuffer) > 0);    // empty string terminates
}

// Read the entity body of given length (after all the headers) into the buffer.
void readEntityBody(EthernetClient & client, int nContentLength, BUFFER & content)
{
  int i;
  char c;

  if (nContentLength >= sizeof(content))
    nContentLength = sizeof(content) - 1;  // Should never happen!

  for (i = 0; i < nContentLength; ++i)
  {
    c = client.read();
//    Serial.print(c); // DEBUG
    content[i] = c;
  }

  content[nContentLength] = 0;  // Null string terminator

//  Serial.print("Content: ");
//  Serial.println(content);
}

// See if we recognize the URI and get its index; or -1 if we don't recognize it.
int GetUriIndex(char * pUri)
{
  int nUriIndex = -1;

  // select the page from the buffer (GET and POST) [start]
  for (int i = 0; i < NUM_URIS; i++)
  {
    if (strcmp_P(pUri, (PGM_P)pgm_read_word(&(http_uris[i]))) == 0)
    {
      nUriIndex = i;
      Serial.print("URI: ");
      Serial.println(pUri);
      break;
    }
  }
//  Serial.print("URI: ");
//  Serial.print(pUri);
//  Serial.print(" Page: ");
//  Serial.println(nUriIndex);

  return nUriIndex;
}

/**********************************************************************************************************************
* Read the next HTTP header record which is CRLF delimited.  We replace CRLF with string terminating null.
***********************************************************************************************************************/
void getNextHttpLine(EthernetClient & client, BUFFER & readBuffer)
{
  char c;
  int bufindex = 0; // reset buffer

  // reading next header of HTTP request
  if (client.connected() && client.available())
  {
    // read a line terminated by CRLF
    readBuffer[0] = client.read();
    readBuffer[1] = client.read();
    bufindex = 2;
    for (int i = 2; readBuffer[i - 2] != '\r' && readBuffer[i - 1] != '\n'; ++i)
    {
      // read full line and save it in buffer, up to the buffer size
      c = client.read();
      if (bufindex < sizeof(readBuffer))
        readBuffer[bufindex++] = c;
    }
    readBuffer[bufindex - 2] = 0;  // Null string terminator overwrites '\r'
  }
}

/**********************************************************************************************************************
*                                                              Send Pages
       Full-Response  = Status-Line         
                        *( General-Header   
                         | Response-Header 
                         | Entity-Header ) 
                        CRLF
                        [ Entity-Body ]   

       Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
       General-Header = Date | Pragma
       Response-Header = Location | Server | WWW-Authenticate
       Entity-Header  = Allow | Content-Encoding | Content-Length | Content-Type
                      | Expires | Last-Modified | extension-header
*
***********************************************************************************************************************/
void sendPage(EthernetClient & client, int nUriIndex, BUFFER & requestContent)
{
  // send HTML header
  sendProgMemAsString(client, (char*)pgm_read_word(&(contents_main[CONT_TOP])));

  // send menu
  sendProgMemAsString(client, (char*)pgm_read_word(&(contents_main[CONT_MENU])));

  // send title
  sendProgMemAsString(client, (char*)pgm_read_word(&(contents_titles[nUriIndex])));

  // send the body for the requested page
  sendUriContentByIndex(client, nUriIndex, requestContent);

  // Append the data sent in the original HTTP request
  client.print("<br />");
  // send POST variables
  client.print(requestContent);

  // send footer
  sendProgMemAsString(client,(char*)pgm_read_word(&(contents_main[CONT_FOOTER])));
}

/**********************************************************************************************************************
*                                                              Send Images
***********************************************************************************************************************/
void sendImage(EthernetClient & client, int nUriIndex, BUFFER & requestContent)
{
  int nImageIndex = nUriIndex - NUM_PAGES - NUM_APIS;

  // send the header for the requested image
  sendUriContentByIndex(client, nUriIndex, requestContent);

  // send the image data
  sendProgMemAsBinary(client, (char *)pgm_read_word(&(data_for_images[nImageIndex])), (int)pgm_read_word(&(size_for_images[nImageIndex])));
}

/**********************************************************************************************************************
*                                                              Send content split by buffer size
***********************************************************************************************************************/
// If we provide string data then we don't need specify an explicit size and can do a string copy
void sendProgMemAsString(EthernetClient & client, const char *realword)
{
  sendProgMemAsBinary(client, realword, strlen_P(realword));
}

// Non-string data needs to provide an explicit size
void sendProgMemAsBinary(EthernetClient & client, const char* realword, int realLen, bool progMem = true)
{
  int remaining = realLen;
  const char * offsetPtr = realword;
  int nSize = sizeof(BUFFER);
  BUFFER buffer;

  while (remaining > 0)
  {
    // print content
    if (nSize > remaining)
      nSize = remaining;      // Partial buffer left to send

    if (progMem) memcpy_P(buffer, offsetPtr, nSize);
    else memcpy(buffer, offsetPtr, nSize);

    if (client.write((const uint8_t *)buffer, nSize) != nSize)
      Serial.println(F("Failed to send data"));

    // more content to print?
    remaining -= nSize;
    offsetPtr += nSize;
  }
}

/**********************************************************************************************************************
*                                                              Send real page content
***********************************************************************************************************************/
// This method takes the contents page identified by nUriIndex, divides it up into buffer-sized
// strings, passes it on for STX substitution and finally sending to the client.
void sendUriContentByIndex(EthernetClient client, int nUriIndex, BUFFER & requestContent)
{
  // Locate the page data for the URI and prepare to process in buffer-sized chunks.
  const char * offsetPtr;               // Pointer to offset within URI for data to be copied to buffer and sent.
  char *pNextString;
  int nSubstituteIndex = -1;            // Count of substitutions so far for this URI
  int remaining;                        // Total bytes (of URI) remaining to be sent
  int nSize = sizeof(BUFFER) - 1;       // Effective size of buffer allowing last char as string terminator
  BUFFER buffer;

  if (nUriIndex < NUM_PAGES)
    offsetPtr = (char*)pgm_read_word(&(contents_pages[nUriIndex]));
  else 
    offsetPtr = (char*)pgm_read_word(&(image_header));

  buffer[nSize] = 0;  // ensure there is always a string terminator
  remaining = strlen_P(offsetPtr);  // Set total bytes of URI remaining

  while (remaining > 0)
  {
    // print content
    if (nSize > remaining)
    {
      // Set whole buffer to string terminator before copying remainder.
      memset(buffer, 0, STRING_BUFFER_SIZE);
      nSize = remaining;      // Partial buffer left to send
    }
    memcpy_P(buffer, offsetPtr, nSize);
    offsetPtr += nSize;
    // We have a buffer's worth of page to check for substitution markers/delimiters.
    // Scan the buffer for markers, dividing it up into separate strings.
    if (buffer[0] == *pStxDelimiter)    // First char is delimiter
    {
      sendSubstitute(client, nUriIndex, ++nSubstituteIndex, requestContent);
      --remaining;
    }
    // First string is either terminated by the null at the end of the buffer
    // or by a substitution delimiter.  So simply send it to the client.
    pNextString = strtok(buffer, pStxDelimiter);
    client.print(pNextString);
    remaining -= strlen(pNextString);
    // Scan for strings between delimiters
    for (pNextString = strtok(NULL, pStxDelimiter); pNextString != NULL && remaining > 0; pNextString = strtok(NULL, pStxDelimiter))
    {
      // pNextString is pointing to the next string AFTER a delimiter
      sendSubstitute(client, nUriIndex, ++nSubstituteIndex, requestContent);
      --remaining;
      client.print(pNextString);
      remaining -= strlen(pNextString);
    }
  }
}

// Call this method in response to finding a substitution character '\002' within some
// URI content to send the appropriate replacement text, depending on the URI index and
// the substitution index within the content.
void sendSubstitute(EthernetClient client, int nUriIndex, int nSubstituteIndex, BUFFER & requestContent)
{
  if (nUriIndex < NUM_PAGES)
  {
    // Page request
    switch (nUriIndex)
    {
      case 1:  // page 2
        client.print("<b>Insert #");
        client.print(nSubstituteIndex);
        client.print("</b>");
        break;
      case 2:  // page 3
        break;
    }
  }
  else
  {
    // Image request
    int nImageIndex = nUriIndex - NUM_PAGES - NUM_APIS;

    switch (nSubstituteIndex)
    {
      case 0:
        // Content-Length value - ie. image size
        char strSize[6];  // Up to 5 digits plus null terminator
        itoa((int)pgm_read_word(&(size_for_images[nImageIndex])), strSize, 10);
        Serial.println(strSize);    // Debug
        client.print(strSize);
        break;
      case 1:
        // Content-Type partial value
        switch (nImageIndex)
        {
          case 0:  // favicon
            client.print("x-icon");
            break;
          case 1:  // led on image
          case 2:  // led off image
            client.print("png");
            break;
        }
    }
  }
}
/**********************************************************************************************************************
*                                                          END OF CODE! WELL DONE!
***********************************************************************************************************************/
