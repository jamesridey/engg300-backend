enum Status {
  UNKNOWN,
  STARTING,
  RUNNING,
  STOPPING,
  WAITING,
  MOVING,
  STOPPED,
  COLLECTING,
  RETURNING,
  ERROR_CANS,
  ERROR_BLUETOOTH
};

class BluetoothInterface {
  public:
    BluetoothInterface() {
      redCans = 0;
      blueCans = 0;
      greenCans = 0;
      
      redCansError = 0;
      blueCansError = 0;
      greenCansError = 0;
      
      stat = UNKNOWN;
    }
    
    void setCanAmount(int r, int g, int b){
      redCans = r;
      greenCans = g;
      blueCans = b;
    }

    void setCanError(int r, int g, int b){
      redCansError = r;
      greenCansError = g;
      blueCansError = b;
    }

    void setStatus(Status s){
      stat = s;
    }

    void setReturnedStatus(int state){
      switch (state) {
        case 0:
          Serial.println("This shouldn't ever be returned");
          break;
        case 1:
          stat = WAITING;
          break;
        case 2:
          stat = MOVING;
          break;
        case 3:
          stat = STOPPED;
          break;
        case 4:
          stat = COLLECTING;
          break;
        case 5:
          stat = RETURNING;
          break;
        default:
          Serial.println("Invalid state returned");
      }
    }

    int getRed(){
      return redCans;
    }

    int getGreen(){
      return greenCans;
    }

    int getBlue(){
      return blueCans;
    }

    void setRed(int red){
		  redCans = red;
    }

    void setGreen(int green){
		  greenCans = green;
    }

    void setBlue(int blue){
		  blueCans = blue;
    }

    Status getStatus(){
      return stat;
    }

    char* getStatusAsString() {
      static char s[64];
      
      switch (stat) {
        case UNKNOWN:
          strcpy(s,"Unknown");  
          break;
        case STARTING:
          strcpy(s,"Starting");
          break;
        case MOVING:
          strcpy(s,"Moving");
          break;
        case STOPPING:
          strcpy(s,"Stopping");
          break;
        case WAITING:
          strcpy(s,"Waiting");
          break;
        case RUNNING:
          strcpy(s,"Running");
          break;
        case STOPPED:
          strcpy(s,"Stopped");
          break;
        case COLLECTING:
          strcpy(s,"Collecting");
          break;
        case RETURNING:
          strcpy(s,"Returning");
          break;
        case ERROR_BLUETOOTH:
          strcpy(s,"Error: Bluetooth communication failure");
          break;
        case ERROR_CANS:
          sprintf(s,"Error: Couldn't collect cans: R:%d, G:%d, B:%d",redCansError,
                                                                     blueCansError,
                                                                     greenCansError);
          break;
        default:
          strcpy(s,"State not defined as string");
      }
      return s;
    }
    
  private:
    unsigned int redCans;
    unsigned int blueCans;
    unsigned int greenCans;

    unsigned int redCansError;
    unsigned int greenCansError;
    unsigned int blueCansError;
    Status stat;
};
